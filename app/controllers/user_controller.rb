class UserController < ApplicationController

  def index
    User.all
  end

  def new
    User.new
  end

  def edit
  end

  def show
    current_user
  end

  def update
  end

  def destroy
  end


end
